//
//  LocationError.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/21/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation

enum LocationError: Error {
    case noPermissions
    case noLocation
}

extension LocationError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noPermissions:
            return NSLocalizedString("We couldn't determine your location as we don't have access to it. Please go to Settings in order to enable it.", comment: "Couldn't find your location")
        case .noLocation:
             return NSLocalizedString("We couldn't determine your location.", comment: "Couldn't find your location")
        }
    }
}

enum UserError: Error {
    case noUser
    case requestPending
    case alreadyAdded
    case userNotDoctor
}

extension UserError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noUser:
            return NSLocalizedString("The requested doctor does not exist.", comment: "The requested user does not exist.")
        case .requestPending:
            return NSLocalizedString("You have already sent a request to this doctor", comment: "You have already sent a request to this doctor")
        case .alreadyAdded:
            return NSLocalizedString("This doctor has already accepted your request", comment: "This doctor has already accepted your request")
        case .userNotDoctor:
            return NSLocalizedString("The specified user is not a doctor", comment: "The specified user is not a doctor")
        }
    }
}
