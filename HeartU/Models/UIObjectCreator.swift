//
//  ObjectCreator.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/19/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit

public class UIObjectCreator{
    
    static func createKeyboardToolBar(view: Any?, selector: Selector?) -> UIToolbar {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: view, action: selector);
        toolbar.setItems([doneButton], animated: true)
        return toolbar
    }
}
