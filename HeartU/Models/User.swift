//
//  User.swift
//  HeartU
//
//  Created by Tefa on 3/24/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation
import Firebase

struct User {
    
    let id: String
    let firstName: String
    let birthday: Date
    let country: String
    let diseases: Array<String>
    let email: String
    let ethnicity: String
    let height: Double
    let weight: Double
    let lastName: String
    let sex: String
    let doctor: Bool
    
    init(child: DataSnapshot) {
        id = child.key
        let dictionaryResult = child.value as! NSDictionary
        firstName = (dictionaryResult.value(forKey: "firstName") as! String)
        birthday = DateHandler.getDate(fromString: dictionaryResult.value(forKey: "birthdate")! as! String)! 
        country = (dictionaryResult.value(forKey: "country") as! String)
        diseases = (dictionaryResult.value(forKey: "diseases") as! Array<String>)
        email = (dictionaryResult.value(forKey: "email") as! String)
        ethnicity = (dictionaryResult.value(forKey: "ethnicity") as? String ?? "N/A")
        height = (Double(dictionaryResult.value(forKey: "height") as! String)!)
        weight = (Double(dictionaryResult.value(forKey: "weight") as! String)!)
        lastName = (dictionaryResult.value(forKey: "lastName") as! String)
        sex = (dictionaryResult.value(forKey: "sex") as! String)
        doctor = (dictionaryResult.value(forKey: "doctor") as? Bool ?? false)
    }
    
}

struct UserBare {
    
    let id: String
    let email: String
    let firstName: String
    let lastName: String
    let accepted: Bool
    
    init(child: DataSnapshot) {
        id = child.key
        let dictionaryResult = child.value as! NSDictionary
        firstName = (dictionaryResult.value(forKey: "firstName") as! String)
        lastName = (dictionaryResult.value(forKey: "lastName") as! String)
        email = (dictionaryResult.value(forKey: "email") as! String)
        accepted = (dictionaryResult.value(forKey: "accepted") as! Bool)
    }
    
}
