//
//  Report.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation
import Firebase

struct Report {
    
    let reportId: String
    let activity: String
    let averageHeartRate: Double
    let heartRate: Array<Double>
    let latitude: String
    let locale: String
    let longitude: String
    let moreInfo: String
    let processed: Bool
    let timestamp: Date
    
    init(child: DataSnapshot) {
        reportId = child.key
        let dictionaryResult = child.value as! NSDictionary

        activity = (dictionaryResult.value(forKey: "activity") as! String)
        averageHeartRate = ((dictionaryResult.value(forKey: "averageHeartRate") as? Double ?? 0) * 100).rounded() / 100
        heartRate = (dictionaryResult.value(forKey: "heartRate") as? Array<Double> ?? [0])
        locale = (dictionaryResult.value(forKey: "locale") as! String?) ?? "Location not available"
        moreInfo = (dictionaryResult.value(forKey: "moreInfo") as! String)
        processed = (dictionaryResult.value(forKey: "processed") as! Bool)
        timestamp = DateHandler.getDate(fromString: (dictionaryResult.value(forKey: "timestamp") as! String))!

        latitude =  ""
        longitude = ""
    }
    
}
