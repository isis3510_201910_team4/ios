//
//  CardTableViewCell.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import SwiftChart

class CardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.backgroundColor = UIColor.clear
            containerView.layer.shadowColor = UIColor.black.cgColor
            containerView.layer.cornerRadius = 4
            containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            containerView.layer.shadowOpacity = 0.3
            containerView.layer.shadowRadius = 1
        }
    }
    
    @IBOutlet weak var clippingView: UIView! {
        didSet {
            clippingView.layer.cornerRadius = 4
            clippingView.backgroundColor = UIColor.clear
            clippingView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var TopStackView: UIStackView!{
        didSet {
            let subview = UIView(frame: self.bounds)
            subview.translatesAutoresizingMaskIntoConstraints = false
            subview.backgroundColor = UIColor.init(hexFromString: "#1fbccc")
            TopStackView.insertSubview(subview, at: 0)
            NSLayoutConstraint.activate([
                leadingAnchor.constraint(equalTo: subview.leadingAnchor),
                trailingAnchor.constraint(equalTo: subview.trailingAnchor),
                topAnchor.constraint(equalTo: subview.topAnchor),
                bottomAnchor.constraint(equalTo: subview.bottomAnchor)
            ])
        }
    }
    
    @IBOutlet weak var EjectionLabel: UILabel!
    
    @IBOutlet weak var DateLabel: UILabel!
    
    @IBOutlet weak var chart: Chart!
    
    @IBOutlet weak var PlaceLabel: UILabel!
    
    var report: Report? {
        didSet {
            if let report = report {
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd, HH:mm"
                DateLabel.text = dateFormatterPrint.string(from: report.timestamp)
                PlaceLabel.text = report.locale
                EjectionLabel.text = "\(report.averageHeartRate) b/min"
            }
            
            clippingView.layer.cornerRadius = 4
            clippingView.backgroundColor = UIColor.white
            clippingView.layer.masksToBounds = true
            
            let series = ChartSeries(report?.heartRate ?? [0, 1, 2])
            series.colors = (
                above: UIColor.white,
                below: UIColor.white,
                zeroLevel: 5
            )
            chart.add(series)
            chart.showXLabelsAndGrid = false
            chart.showYLabelsAndGrid = false
            chart.bottomInset = 10
            chart.topInset = 12
            chart.axesColor = UIColor.clear
            chart.gridColor = UIColor.clear
            chart.labelColor = UIColor.clear
            chart.tintColor = UIColor.clear
            chart.highlightLineColor = UIColor.clear
        }
    }
    @IBOutlet weak var MoreInfo: UILabel!{
        didSet{
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "info")
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string: "")
            myString.append(attachmentString)
            MoreInfo.attributedText = myString
            
            MoreInfo.isUserInteractionEnabled = true
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapLabel(tapGestureRecognizer:)))
            MoreInfo.addGestureRecognizer(tapGesture)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        chart.removeAllSeries()
    }
    
    @objc func userDidTapLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        self.parentContainerViewController()?.performSegue(withIdentifier: "showReportDetailSegue", sender: report!)
    }
    
    @IBOutlet weak var MiddleLabel: UILabel!{
        didSet{
            MiddleLabel.layer.borderColor = UIColor.init(hexFromString: "#D0D0D0").cgColor
            MiddleLabel.layer.borderWidth = 1
        }
    }
    
}
