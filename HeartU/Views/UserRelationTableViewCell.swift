//
//  UserRelationTableViewCell.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/22/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit

class UserRelationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var EmailLabel: UILabel!
    @IBOutlet weak var AcceptButton: UIButton!{
        didSet {
//            AcceptButton.layer.cornerRadius = AcceptButton.frame.width/2
//            AcceptButton.clipsToBounds = true
//            AcceptButton.layer.borderWidth = 1
            AcceptButton.imageView?.clipsToBounds = true
//            AcceptButton.layer.masksToBounds = true
//            AcceptButton.layer.borderColor = UIColor.clear.cgColor
        }
    }
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var NameLabelRequest: UILabel!
    @IBOutlet weak var EmailLabelRequest: UILabel!
    
    var user: UserBare!{
        didSet {
            if NameLabel != nil {
                NameLabel.text = "\(user.firstName) \(user.lastName)"
                EmailLabel.text = user.email
            } else {
                NameLabelRequest.text = "\(user.firstName) \(user.lastName)"
                EmailLabelRequest.text = user.email
            }
        }
    }

    @IBAction func onRejectTap(_ sender: Any) {
        let parentController = self.parentContainerViewController() as? PatientRequestsTableViewController
        UserHandler.cancelUserRequest(patientID: user.id).catch { (err) in
            ErrorHandler.showErrorMessage(view: parentController!, error: err)
        }
    }
    
    @IBAction func onConfirmTap(_ sender: Any) {
        let parentController = self.parentContainerViewController() as? PatientRequestsTableViewController
        LoadingSpinner.showSpinner(onView: self)
        UserHandler.acceptUserRequest(patientID: user.id).then { (none) in
            LoadingSpinner.removeSpinner()
            parentController?.navigationController?.popViewController(animated: true)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
