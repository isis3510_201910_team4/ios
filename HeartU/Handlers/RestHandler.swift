//
//  RestHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation
import Promises
import Alamofire

class RestHandler {
    
    static let BASE_URL = "https://video-receiver.herokuapp.com/api/"
//    static let BASE_URL = "http://localhost:9090/api/"
//    static let BASE_URL = "http://192.168.0.3:9000/api/"
    
    static func GETLocationData(location: LocationHandler.UserLocation) -> Promise<String>{
        return Promise<String>(on: .global(qos: .background)) { (fullfill, reject) in
            let urlString = "https://nominatim.openstreetmap.org/reverse?format=json&lat=\(location.lat)&lon=\(location.long)&zoom=18"
            var request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
                do {
                    if let error = error {
                        fullfill("NA")
                    }
                    let json = try JSONSerialization.jsonObject(with: data!) as! NSDictionary
                    let location = json.object(forKey: "address") as? NSDictionary
                    let locationString = "\(location?.object(forKey: "city") as! String), \(location?.object(forKey: "country") as! String)"
                    fullfill(locationString)
                } catch  {
                    print("Unexpected error: \(error).")
                }
            }).resume()
        }
    }
    
    static func postNew(videoUrl: Data, reportId: String){
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(videoUrl, withName: "video", fileName: "video", mimeType: "video/mp4")
        },
            to: "\(BASE_URL)videos/\(UserHandler.getCurrentUserId()!)/\(reportId)",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
}
