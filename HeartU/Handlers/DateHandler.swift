//
//  DateHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/25/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit

class DateHandler {
    
    static let dateFormatStringCT: String = "yyyy-MM-dd HH:mm:ss.SSS"
    
    static func getCurrentDateFormatted() -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = dateFormatStringCT
        return dateFormatterPrint.string(from: Date.init())
    }
    
    static func formatDate(toFormat: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = dateFormatStringCT
        return dateFormatterPrint.string(from: toFormat)
    }
    
    static func formatDateNoHour(toFormat: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy"
        return dateFormatterPrint.string(from: toFormat)
    }
    
    static func formatDateDDMM(toFormat: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM"
        return dateFormatterPrint.string(from: toFormat)
    }
    
    static func getDate(fromString: String) -> Date? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = dateFormatStringCT
        return dateFormatterPrint.date(from: fromString)
    }
}
