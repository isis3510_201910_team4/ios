//
//  FormHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/19/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit

public class FormHandler{
    
    static func verifyIfFieldsNotEmpty(view: UIViewController, textFields: UITextField?...) -> Bool {
        for singleTextField in textFields {
            guard let textField = singleTextField, let text = textField.text, !text.isEmpty, !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                //Field is null or has no text on it.
                ErrorHandler.showErrorMessage(view: view, errorMessage: "\(singleTextField!.placeholder!) is empty.")
                return false;
            }
        }
        return true
    }
    
    static func generateListOfDiseases(switches: Array<UISwitch?>, names: Array<String>) -> Array<String> {
        var array:Array<String> = []
        for (index, singleSwitch) in switches.enumerated() {
            if let singleSwitch = singleSwitch {
                if singleSwitch.isOn {
                    array.append(names[index])
                }
            }
        }
        return array
    }
}
