//
//  ReportDataHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/24/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation
import Firebase
import Promises

public class ReportDataHandler {
    
    static var ref: DatabaseReference!
    static let amountOfMeasuresPerReport: Int = 300;
    
    static func getLastSixReports(userId: String) -> Promise<Array<Report>>{
        return Promise<Array<Report>>(on: .global(qos: .background)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            self.ref.child("reports").child(userId).queryOrdered(byChild: "timestamp").queryLimited(toLast: 6).observeSingleEvent(of: .value, with: { (snapshot) in
                
                var data = [Report]()
                for child in snapshot.children.allObjects{
                    let reportConverted = Report(child: (child as! DataSnapshot))
                    data.insert(reportConverted, at: 0)
                }
                fullfill(data)
            }) { (error) in
                reject(error)
            }
        }
    }
    
    static func getLastReportUser(userId: String) -> Promise<DataSnapshot>{
        return Promise<DataSnapshot>(on: .global(qos: .background)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            self.ref.child("reports").child(userId).queryOrdered(byChild: "timestamp").observeSingleEvent(of: .value) { (snapshot) in
                fullfill(snapshot)
            }
        }
    }
    
    static func removeReport(userId: String, reportId: String) -> Void {
        self.ref = Database.database().reference()
        self.ref.child("reports").child(userId).child(reportId).removeValue()
    }
    
    static func createReport(activity: String, moreInfo: String) -> Promise<String> {
        return Promise<String>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            createReportFirebase(userId: UserHandler.getCurrentUserId()!, activity: activity, moreInfo: moreInfo)
                .then{ (result) in fullfill(result)}
                .catch{ (error) in reject(error)}
        }
    }
    
    static func createReportFirebase(userId: String, activity: String, moreInfo: String) -> Promise<String> {
        return Promise<String>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            let locationHandler = LocationHandler()
            var lat, long: String?
            var locationStr = "Location N/A"
            //Get current user location
            locationHandler.getCurrentLocation().then({ userLocation in
                lat = userLocation.lat.description
                long = userLocation.long.description
            }).then({ (userLocation) in RestHandler.GETLocationData(location: userLocation)
            }).then({ (locationString) in
                locationStr = locationString
            }).catch({ (error) in
                print("Error getting location \(error)")
            }).always {
                let newReport = [
                    "activity" : activity,
                    "latitude" : lat,
                    "locale" : locationStr,
                    "longitude" : long,
                    "moreInfo" : moreInfo,
                    "processed" : false,
                    "timestamp" : DateHandler.getCurrentDateFormatted()
                    ] as [String : Any?]
                let newRef = self.ref.child("reports").child(userId).childByAutoId()
                newRef.setValue(newReport){
                    (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        reject(error)
                        print("Data could not be saved: \(error).")
                    } else {
                        fullfill(newRef.key!)
                    }
                }
            }
        }
    }
    
    static func subscribeToUserReports(userId: String, updateData: @escaping (Array<Report>) -> Void) {
        self.ref = Database.database().reference()
        self.ref.child("reports").child(userId).observe(DataEventType.value, with: { (snapshot) in
            var data = [Report]()
            for child in snapshot.children.allObjects{
                let childSnapShot = (child as! DataSnapshot)
                let userConverted = Report(child: childSnapShot)
                
                data.insert(userConverted, at: 0)
            }
            updateData(data)
        })
    }
}
