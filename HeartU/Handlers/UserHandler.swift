//
//  UserHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/21/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation
import Firebase
import Promises

class UserHandler {
    
    static var ref:DatabaseReference!
    
    static func getLoggedInUser() -> Promise<User> {
        return Promise<User>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            let userID : String? = Auth.auth().currentUser?.uid
            if userID == nil {
                reject(UserError.noUser)
            } else {
                self.ref = Database.database().reference()
                self.ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
                    let userConverted = User(child: snapshot)
                    fullfill(userConverted)
                }){ (error) in
                    reject(error)
                }
            }
        }
    }
    
    static func getUser(userID: String) -> Promise<User> {
        return Promise<User>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            self.ref.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
                let userConverted = User(child: snapshot)
                fullfill(userConverted)
            }){ (error) in
                reject(error)
            }
        }
    }
    
    static func getUser(userEmail: String) -> Promise<User> {
        return Promise<User>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            self.ref.child("users").queryOrdered(byChild: "email").queryEqual(toValue: userEmail).observeSingleEvent(of: .value, with: { (snapshot) in
                if (snapshot.childrenCount == 0){
                    reject(UserError.noUser)
                } else {
                    let userConverted = User(child: snapshot.children.nextObject() as! DataSnapshot)
                    fullfill(userConverted)
                }
            }){ (error) in
                reject(error)
            }
        }
    }
    
    static func getAllPatientUsers(accepted: Bool) -> Promise<Array<UserBare>>{
        return Promise<Array<UserBare>>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            let userID : String? = Auth.auth().currentUser?.uid
            self.ref.child("relations").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
                var data = [UserBare]()
                for child in snapshot.children.allObjects{
                    let childSnapShot = (child as! DataSnapshot)
                    let userConverted = UserBare(child: childSnapShot)
                    if userConverted.accepted == accepted {
                        data.insert(userConverted, at: 0)
                    }
                }
                fullfill(data)
            }) { (error) in
                reject(error)
            }
        }
    }
    
    static func createUserRequest(_ doctorEmail: String,_ doctors: Array<UserBare>,_ requests: Array<UserBare>) -> Promise<Any?>{
        return Promise<Any?>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            if doctors.contains(where: { (doc) -> Bool in
                return doc.email == doctorEmail
            }){ reject(UserError.alreadyAdded) }
            else if requests.contains(where: { (doc) -> Bool in
                return doc.email == doctorEmail
            }){ reject(UserError.requestPending) }
            else{
                self.ref = Database.database().reference()
                self.getUser(userEmail: doctorEmail).then({ (doctorUser) in
                    if !doctorUser.doctor {
                        reject(UserError.userNotDoctor)
                    }
                    else {
                        let userID : String? = Auth.auth().currentUser?.uid
                        var relation = ["accepted" : false, "email": doctorEmail, "firstName": doctorUser.firstName, "lastName": doctorUser.lastName] as [String : Any]
                    self.ref.child("relations").child(userID!).child(doctorUser.id).setValue(relation){
                            (error:Error?, ref:DatabaseReference) in
                            if let error = error {
                                reject(error)
                            } else {
                                self.getLoggedInUser().then({ (loggedUser) in
                                    relation = ["accepted" : false, "email": loggedUser.email, "firstName": loggedUser.firstName, "lastName": loggedUser.lastName] as [String : Any]
     self.ref.child("relations").child(doctorUser.id).child(userID!).setValue(relation){
                                        (error:Error?, ref:DatabaseReference) in
                                        if let error = error {
                                            reject(error)
                                        } else {
                                            fullfill(nil)
                                        }
                                    }
                                }).catch({ (err) in reject(err) })
                            }
                        }
                    }
                }).catch({ (err) in reject(err) })
            }
        }
    }
    
    static func acceptUserRequest(patientID: String) -> Promise<Any?>{
        return Promise<Any?>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            let userID : String? = Auth.auth().currentUser?.uid
            self.ref.child("relations").child(userID!).child(patientID).updateChildValues(["accepted" : true]){
                (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    reject(error)
                } else {
 self.ref.child("relations").child(patientID).child(userID!).updateChildValues(["accepted" : true]){
                        (error:Error?, ref:DatabaseReference) in
                        if let error = error {
                            reject(error)
                        } else {
                            fullfill(nil)
                        }
                    }
                }
            }
        }
    }
    
    static func cancelUserRequest(patientID: String) -> Promise<Any?>{
        return Promise<Any?>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            self.ref = Database.database().reference()
            let userID : String? = Auth.auth().currentUser?.uid
            self.ref.child("relations").child(userID!).child(patientID).removeValue(){
                (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    reject(error)
                } else {
                    self.ref.child("relations").child(patientID).child(userID!).removeValue(){
                        (error:Error?, ref:DatabaseReference) in
                        if let error = error {
                            reject(error)
                        } else {
                            fullfill(nil)
                        }
                    }
                }
            }
        }
    }
    
    static func getCurrentUserId() -> String? {
        return Auth.auth().currentUser?.uid
    }
    
    static func subscribeToUserPatients(accepted: Bool, updateData: @escaping (Array<UserBare>) -> Void) {
        self.ref = Database.database().reference()
        let userID : String? = Auth.auth().currentUser?.uid
        self.ref.child("relations").child(userID!).observe(DataEventType.value, with: { (snapshot) in
            print("HERE")
            var data = [UserBare]()
            for child in snapshot.children.allObjects{
                let childSnapShot = (child as! DataSnapshot)
                let userConverted = UserBare(child: childSnapShot)
                if userConverted.accepted == accepted {
                    data.insert(userConverted, at: 0)
                }
            }
            updateData(data)
        })
    }
}
