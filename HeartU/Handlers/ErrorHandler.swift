//
//  ErrorHandler.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/18/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Promises

class ErrorHandler{
    
    static func showErrorMessage(view: UIViewController, errorMessage:String) {
        let alertController = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel)
        alertController.addAction(alertAction)
        view.present(alertController, animated: true, completion: nil)
    }
    
    static func showErrorMessagePromise(view: UIViewController, errorMessage:String) -> Promise<Any?> {
        return Promise<Any?>(on: .global(qos: .background)) { (fullfill, reject) in
            let alertController = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: {_ in
                CATransaction.setCompletionBlock({
                    fullfill(nil)
                })
            })
            alertController.addAction(alertAction)
            view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showErrorMessage(view: UIViewController, error:Error) {
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel)
        alertController.addAction(alertAction)
        view.present(alertController, animated: true, completion: nil)
    }
    
    static func confirmDelete(view: UIViewController, messageStr: String, onDeleteFunc: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: "Delete", message: messageStr, preferredStyle: .actionSheet)
        let DeleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: onDeleteFunc)
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)

        view.present(alert, animated: true, completion: nil)
    }

}
