import UIKit
import MapKit
import CoreLocation
import Promises

class LocationHandler: NSObject, CLLocationManagerDelegate{
    
    var locationManager: CLLocationManager!
    
    public struct UserLocation {
        var lat: CLLocationDegrees
        var long: CLLocationDegrees
    }
    
    var lat: CLLocationDegrees?
    var long: CLLocationDegrees?
    var noLocation = false

    override init() {
        super.init()
        OperationQueue.main.addOperation {
            self.locationManager = CLLocationManager()
            if CLLocationManager.locationServicesEnabled() == true {
                if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                    self.locationManager.requestWhenInUseAuthorization()
                }
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.delegate = self
                self.locationManager.startUpdatingLocation()
            } else {
                self.noLocation = true
                print("Please turn on location services or GPS")
            }
        }
    }
    
    //MARK:- CLLocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        self.lat = locations[0].coordinate.latitude
        self.long = locations[0].coordinate.longitude
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.noLocation = true
    }
    
    func getCurrentLocation() -> Promise<UserLocation> {
        return Promise<UserLocation>(on: .global(qos: .background)) { (fullfill, reject) in
            while (self.lat == nil && self.long == nil && self.noLocation == false){
                sleep(1)
            }
            if (self.noLocation == true){
                reject(LocationError.noPermissions)
            } else {
                fullfill(UserLocation.init(lat: self.lat!, long: self.long!))
            }
        }
    }
}
