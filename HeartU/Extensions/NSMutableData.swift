//
//  NSMutableData.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/27/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import Foundation

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
