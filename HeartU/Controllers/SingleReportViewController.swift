//
//  SingleReportViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/22/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import SwiftChart

class SingleReportViewController: UIViewController, ChartDelegate {
    
    //Label outlets
    @IBOutlet weak var HeartRate: UILabel!
    @IBOutlet weak var ActivityLabel: UILabel!
    @IBOutlet weak var Location: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    //Chart outlets
    @IBOutlet weak var labelLeadingMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var chart: Chart!
    @IBOutlet weak var label: UILabel!
    
    let MAX_NUM_X_LABELS: Int = 15
    
    fileprivate var labelLeadingMarginInitialConstant: CGFloat!
    
    var report: Report?
    var isDoctorViewing: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelLeadingMarginInitialConstant = labelLeadingMarginConstraint.constant
        initializeChart()
        if (!isDoctorViewing){
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(share(sender:)))
        } else if (report?.moreInfo != ""){
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Extra info", style: .plain, target: self, action: #selector(extraInfo(sender:)))
        }
        if let report = report {
            HeartRate.text = "\(report.averageHeartRate) b/min"
            ActivityLabel.text = "\(report.activity)"
            Location.text = "\(report.locale)"
            DateLabel.text = "\(DateHandler.formatDate(toFormat: report.timestamp))"
        }
    }
    
    @objc func extraInfo(sender:UIView){
        let alertController = UIAlertController(title: "Report information", message:
            report?.moreInfo, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Close", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func share(sender:UIView){
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsEndImageContext()
        
        if let report = report {
            let textToShare = "Check out the report I took using the HeartU app: \n - Average Heart rate = \(report.averageHeartRate) b/min \n - Activity = \(report.activity) \n - Additional information = \(report.moreInfo) \n This report was taken on \(DateHandler.formatDate(toFormat: report.timestamp)) at \(report.locale) "
            
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //Excluded Activities
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func initializeChart() {
        chart.delegate = self
        
        // Initialize data series and labels
        let stockValues: [Double] = report!.heartRate
        
        var seriesData: [Double] = []
        var labels: [Double] = []
        var labelsAsString: Array<String> = []
        
        // Date formatter to retrieve the month names
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        
        for value in stockValues {
            seriesData.append(value)
        }
        
        let stepSize = seriesData.count / MAX_NUM_X_LABELS
        for value in stockValues {
            seriesData.append(value)
        }
        
        let series = ChartSeries(seriesData)
        series.area = true
        
        // Configure chart layout
        
        chart.lineWidth = 0.5
        chart.labelFont = UIFont.systemFont(ofSize: 12)
        chart.xLabels = labels
        chart.xLabelsFormatter = { (labelIndex: Int, labelValue: Double) -> String in
            return labelsAsString[labelIndex]
        }
        chart.xLabelsTextAlignment = .center
        chart.yLabelsOnRightSide = true
        // Add some padding above the x-axis
        chart.minY = seriesData.min()! - 5
        
        chart.add(series)
        
    }
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Double, left: CGFloat) {
        
        if let value = chart.valueForSeries(0, atIndex: indexes[0]) {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.minimumFractionDigits = 2
            numberFormatter.maximumFractionDigits = 2
            label.text = numberFormatter.string(from: NSNumber(value: value))
            
            // Align the label to the touch left position, centered
            var constant = labelLeadingMarginInitialConstant + left - (label.frame.width / 2)
            
            // Avoid placing the label on the left of the chart
            if constant < labelLeadingMarginInitialConstant {
                constant = labelLeadingMarginInitialConstant
            }
            
            // Avoid placing the label on the right of the chart
            let rightMargin = chart.frame.width - label.frame.width
            if constant > rightMargin {
                constant = rightMargin
            }
            
            labelLeadingMarginConstraint.constant = constant
            
        }
        
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        label.text = ""
        labelLeadingMarginConstraint.constant = labelLeadingMarginInitialConstant
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
    
}
