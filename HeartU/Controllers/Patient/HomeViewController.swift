//
//  HomeViewController.swift
//  HeartU
//
//  Created by Tefa on 3/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import Charts

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var ref: DatabaseReference!
    var dataDoctors:Array<UserBare> = []
    var dataRequests:Array<UserBare> = []
    
    @IBOutlet weak var BarChart: BarChartView!
    @IBOutlet weak var doctorsTableView: UITableView!
    @IBOutlet weak var requestsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDoctorsData()
        getRequestsData()
        self.doctorsTableView.delegate = self
        self.doctorsTableView.dataSource = self
        self.requestsTableView.delegate = self
        self.requestsTableView.dataSource = self
        
        ReportDataHandler.getLastSixReports(userId: UserHandler.getCurrentUserId()!).then { (reports) in
//            let reports = reports[0...6]
            print(reports.count)
            // Initialize an array to store chart data entries (values; y axis)
            var reportEntries = [ChartDataEntry]()
            
            // Initialize an array to store months (labels; x axis)
            var reportDates = [String]()
            
            var i = 0
            for report in reports {
                let reportEntry = BarChartDataEntry(x: Double(i), y: report.averageHeartRate)
                reportEntries.append(reportEntry)
                reportDates.append(DateHandler.formatDateDDMM(toFormat: report.timestamp))
                i += 1
            }
            
            // Create bar chart data set containing salesEntries
            let chartDataSet = BarChartDataSet(entries: reportEntries, label: "Heart rate")
            chartDataSet.colors = [UIColor.init(hexFromString: "#333232"), UIColor.init(hexFromString: "#5D536B"), UIColor.init(hexFromString: "#7D6B91"), UIColor.init(hexFromString: "#989FCE"), UIColor.init(hexFromString: "#8CA0D7")]
            
            // Create bar chart data with data set and array with values for x axis
            let chartData = BarChartData(dataSets: [chartDataSet])
            
            // Set bar chart data to previously created data
            self.BarChart.data = chartData
            self.BarChart.xAxis.labelPosition = .bottom
            self.BarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: reportDates)
            self.BarChart.legend.enabled = false
            self.BarChart.rightAxis.enabled = false
            self.BarChart.animate(xAxisDuration: 1.5, easingOption: .easeInOutBack)
            
            
//            self.BarChart.left
//            self.BarChart.leftAxis.axisMinValue = 0.0
//            self.BarChart.leftAxis.axisMaxValue = 1000.0
        }
        
        
    }    
    
    @IBAction func onAddNewDoctorTap(_ sender: Any) {
        let alert = UIAlertController(title: "Send a request to a new doctor", message: "", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Doctor's email address"
        }
        
        alert.addAction(UIAlertAction(title: "Send request", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0]
            
            UserHandler.createUserRequest(textField.text!, self.dataDoctors, self.dataRequests).then({ _ in
                self.getRequestsData()
            }).catch({ (err) in
                ErrorHandler.showErrorMessage(view: self, error: err)
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TakingExamViewController {
            if let view = sender as? UIViewController {
                destination.parentController = view
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == doctorsTableView{
            return dataDoctors.count
        } else if tableView == requestsTableView{
            return dataRequests.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userInfoCell", for: indexPath) as? UserRelationTableViewCell else {
            fatalError("No CardTableViewCell for cardCell id")
        }
        
        if tableView == doctorsTableView {
            let user = dataDoctors[indexPath.row]
            cell.user = user
        } else if tableView == requestsTableView {
            let user = dataRequests[indexPath.row]
            cell.user = user
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func getDoctorsData(){
        UserHandler.subscribeToUserPatients(accepted: true, updateData: setDoctorsData)
    }
    
    func setDoctorsData(users: Array<UserBare>){
        self.dataDoctors = []
        self.dataDoctors.append(contentsOf: users)
        self.doctorsTableView.reloadData()
    }
    
    func getRequestsData(){
        UserHandler.subscribeToUserPatients(accepted: false, updateData: setRequestsData)
    }
    
    func setRequestsData(users: Array<UserBare>){
        self.dataRequests = []
        self.dataRequests.append(contentsOf: users)
        self.requestsTableView.reloadData()
    }
    
}
