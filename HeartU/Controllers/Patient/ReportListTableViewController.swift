//
//  ReportListTableViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import SwiftChart
import EmptyDataSet_Swift

class ReportListTableViewController: UITableViewController, EmptyDataSetSource, EmptyDataSetDelegate {

    var data:Array<Report> = []
    var patient: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.allowsSelection = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.backgroundColor = UIColor.init(hexFromString: "#61646c")
        self.tableView.backgroundView?.backgroundColor =  UIColor.init(hexFromString: "#61646c")
        self.tableView.contentInset = UIEdgeInsets(top: 5,left: 0,bottom: 0,right: 0)
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        getReportsData()
    }
    
    //Firebase DB subscription methods
    func getReportsData(){
        ReportDataHandler.subscribeToUserReports(userId: UserHandler.getCurrentUserId()!, updateData: setReportsData)
    }
    func setReportsData(reports: Array<Report>){
        self.data = []
        self.data.append(contentsOf: reports)
        self.tableView.reloadData()
    }
    
    
    //Detete function
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            ErrorHandler.confirmDelete(view: self, messageStr: "Are you sure you want to permanently delete this report?", onDeleteFunc: {(alert: UIAlertAction) -> Void in
                ReportDataHandler.removeReport(userId: (Auth.auth().currentUser?.uid)!, reportId: self.data[indexPath.row].reportId)
                self.data.remove(at: indexPath.row)
                tableView.reloadData()
            })
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    @IBAction func onAddTap(_ sender: Any) {
        performSegue(withIdentifier: "takeExamFromReports", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as? CardTableViewCell else {
            fatalError("No CardTableViewCell for cardCell id")
        }
        cell.backgroundColor = UIColor.init(hexFromString: "#61646c")
        // Put data into the cell
        cell.report = data[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showReportDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SingleReportViewController {
            if let report = sender as? Report {
                destination.report = report
                destination.isDoctorViewing = false
            }
        }
        if let destination = segue.destination as? TakingExamViewController {
            if let view = sender as? UITableViewController {
                destination.parentController = view
            }
        }
    }
    
    //Empty data set functions
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return -100
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Welcome"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline),
                     NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Tap + on the top right corner to start taking your first exam."
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body), NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "heartUWhite")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let ac = UIAlertController(title: "Button tapped!", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Hurray", style: .default))
        present(ac, animated: true)
    }

}
