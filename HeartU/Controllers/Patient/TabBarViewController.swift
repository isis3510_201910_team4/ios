//
//  TabBarViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/19/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 0
    }

}

extension TabBarViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
