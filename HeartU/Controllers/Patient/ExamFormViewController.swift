//
//  ExamFormViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/24/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit

class ExamFormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var ActivityField: UITextField!
    @IBOutlet weak var SecondStackView: UIStackView!
    @IBOutlet weak var ThirdStackView: UIStackView!
    @IBOutlet weak var MoreInfoLabel: UITextField!
    
    let activityPicker = UIPickerView()
    let activityList = ["Resting", "Just woke up", "Exercising", "Taking drugs or alcohol"]
    override func viewDidLoad() {
        super.viewDidLoad()
        activityPicker.delegate = self
        ActivityField.inputAccessoryView = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditingActivity))
        ActivityField.inputView = activityPicker
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collapseSection(stackView: SecondStackView)
        collapseSection(stackView: ThirdStackView)
    }
    
    @objc func doneEditingActivity(){
        let row = activityPicker.selectedRow(inComponent: 0);
        ActivityField.text = activityList[row];
        doneEditingActivitySpecific()
    }
    
    func doneEditingActivitySpecific(){
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ActivityField.text = activityList[row];
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activityList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return activityList[row]
    }
    
    @IBAction func onSection1Tap(_ sender: Any) {
        guard let sender = sender as? UIButton else { return }
        guard let middleStackView = sender.superview as? UIStackView else { return }
        guard let stackView = middleStackView.superview as? UIStackView else { return }
        guard stackView.arrangedSubviews.count > 1 else { return }
        collapseSection(stackView: stackView)
    }
    
    @IBAction func onFirstContinueTap(_ sender: Any) {
        guard let sender = sender as? UIButton else { return }
        guard let middleStackView = sender.superview as? UIStackView else { return }
        guard let stackView = middleStackView.superview as? UIStackView else { return }
        guard stackView.arrangedSubviews.count > 1 else { return }
//        print(activityPicker.selectedRow(inComponent: 0))
//        if activityPicker.isFocused {
//            doneEditingActivity()
//        }
        doneEditingActivitySpecific()
        collapseSection(stackView: stackView)
        self.collapseSection(stackView: self.SecondStackView, open: true)
    }
    
    @IBAction func onSecondContinueTap(_ sender: Any) {
        guard let sender = sender as? UIButton else { return }
        guard let middleStackView = sender.superview as? UIStackView else { return }
        guard let stackView = middleStackView.superview as? UIStackView else { return }
        guard stackView.arrangedSubviews.count > 1 else { return }
        collapseSection(stackView: stackView)
        doneEditingActivitySpecific()
        self.collapseSection(stackView: self.ThirdStackView, open: true)
        
    }
    
    @IBAction func onReadyTap(_ sender: Any) {
        if (!FormHandler.verifyIfFieldsNotEmpty(view: self, textFields: ActivityField)){
            return
        }
        performSegue(withIdentifier: "showTakingExam", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TakingExamViewController {
            if let view = sender as? ExamFormViewController {
                destination.parentController = view
                destination.activityString = ActivityField.text
                destination.moreInfoString = MoreInfoLabel.text
            }
        }
    }
    
    func collapseSection(stackView: UIStackView, open: Bool? = nil) {
        let sectionIsCollapsed = stackView.arrangedSubviews[1].isHidden
        
        for i in 1..<stackView.arrangedSubviews.count {
            DispatchQueue.main.asyncAfter(deadline: .now() + (0.1*(Double(i)-1)), execute: {
                UIView.animate(withDuration: 0.1) {
                    if let open = open {
                      stackView.arrangedSubviews[i].isHidden = !open
                    } else {
                      stackView.arrangedSubviews[i].isHidden = !sectionIsCollapsed
                    }
                }
            })
        }
    }

}
