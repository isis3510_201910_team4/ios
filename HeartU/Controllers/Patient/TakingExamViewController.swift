//
//  TakingExamViewController.swift
//  HeartU
//
//  Created by Tefa on 3/20/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import Promises
import AVFoundation
import MobileCoreServices

class TakingExamViewController:  UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var controller = UIImagePickerController()
    let videoFileName = "/video.mp4"
    var activityString: String!
    var moreInfoString: String!
    
    var parentController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.changeStatusBarStatus(newStatus: false)
        takeVideo(self);
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
            let videoData = try? Data(contentsOf: selectedVideo)
            self.createExam().then { (id) in
                RestHandler.postNew(videoUrl: videoData!, reportId: id)
            }
        }
        
        picker.dismiss(animated: true){
            ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "The video was successfully uploaded, we will let you know once it has been processed.").then({ _ in
                self.returnToReportListController()
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true){
            self.returnToReportListController()
        }
    }
    
    @objc func videoSaved(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
        if let theError = error {
            print("error saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
            })
        }
    }
    
    func takeVideo(_ sender: Any) {
        verifyVideoPermissions().then({ (authorized) in
            if !authorized {
                self.returnToReportListController()
                return
            }
            self.verifyAudioPermissions().then({ (audioAuth) in
                if !audioAuth {
                    self.returnToReportListController()
                    return
                }
                self.controller.sourceType = .camera
                self.controller.cameraDevice = .front
                self.controller.mediaTypes = [kUTTypeMovie as String]
                self.controller.delegate = self
                self.present(self.controller, animated: true, completion: nil)
            })
        })
    }
    
    func returnToReportListController() -> Void{
        if let parent = self.parentController as? ExamFormViewController {
                self.changeStatusBarStatus(newStatus: true)
                self.navigationController?.popViewController(animated: false)
                parent.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func viewLibrary(_ sender: Any) {
        // Display Photo Library
        controller.sourceType = UIImagePickerController.SourceType.photoLibrary
        controller.mediaTypes = [kUTTypeMovie as String]
        controller.delegate = self
        
        present(controller, animated: true, completion: nil)
    }
    
    func createExam() -> Promise<String> {
        return Promise<String>(on: .global(qos: .userInitiated)) { (fullfill, reject) in
            ReportDataHandler.createReport(activity: self.activityString, moreInfo: self.moreInfoString)
                .then{ (id) in fullfill(id) }
                .catch{ (error) in reject(error) }
        }
    }
    

    
    func changeStatusBarStatus(newStatus: Bool){
        self.navigationController?.tabBarController?.tabBar.items?[0].isEnabled = newStatus
        self.navigationController?.tabBarController?.tabBar.items?[1].isEnabled = newStatus
        self.navigationController?.tabBarController?.tabBar.items?[2].isEnabled = newStatus
    }
    
    func verifyVideoPermissions() -> Promise<Bool> {
        return Promise<Bool>(on: .global(qos: .userInteractive)) { (fullfill, reject) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                switch AVCaptureDevice.authorizationStatus(for: .video) {
                case .authorized: // The user has previously granted access to the camera.
                    fullfill(true);
                case .notDetermined: // The user has not yet been asked for camera access.
                    AVCaptureDevice.requestAccess(for: .video) { granted in
                        if (!granted){
                            ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable camera access. Please go to Settings -> HeartU to enable it.").then({ _ in
                                fullfill(false)
                            })
                        } else {
                            fullfill(true)
                        }
                    }
                    
                case .denied: // The user has previously denied access.
                    ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable camera access. Please go to Settings -> HeartU to enable it.").then({ _ in
                        fullfill(false)
                    })
                case .restricted: // The user can't grant access due to restrictions.
                    ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable camera access. Please go to Settings -> HeartU to enable it.").then({ _ in
                        fullfill(false)
                    })
                }
            }
            else {
                ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "Your device doesn't have a camera, we will add new ways to measure your heart rate in the future.").then({ _ in
                    fullfill(false)
                })
            }
        }
        
    }
    
    func verifyAudioPermissions() -> Promise<Bool> {
        return Promise<Bool>(on: .global(qos: .userInteractive)) { (fullfill, reject) in
            switch AVCaptureDevice.authorizationStatus(for: .audio) {
            case .authorized: // The user has previously granted access to the camera.
                fullfill(true);
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .audio) { granted in
                    if (!granted){
                        ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable microphone access. Please go to Settings -> HeartU to enable it.").then({ _ in
                            fullfill(false)
                        })
                    } else {
                        fullfill(true)
                    }
                }
                
            case .denied: // The user has previously denied access.
                ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable microphone access. Please go to Settings -> HeartU to enable it.").then({ _ in
                    fullfill(false)
                })
            case .restricted: // The user can't grant access due to restrictions.
                ErrorHandler.showErrorMessagePromise(view: self, errorMessage: "In order to take the exam you will need to enable microphone access. Please go to Settings -> HeartU to enable it.").then({ _ in
                    fullfill(false)
                })
            }
        }
    }
    
}
