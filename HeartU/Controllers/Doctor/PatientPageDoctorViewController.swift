//
//  PatientPageDoctorViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/23/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift
import Firebase
import SwiftChart
import TagListView

class PatientPageDoctorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var SexLabel: UILabel!
    @IBOutlet weak var HeightLabel: UILabel!
    @IBOutlet weak var EthnicityLabel: UILabel!
    @IBOutlet weak var WeightLabel: UILabel!
    @IBOutlet weak var CountryLabel: UILabel!
    @IBOutlet weak var EmailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var userDiseases: TagListView!
    var data:Array<Report> = []
    var patient: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(patient.firstName) \(patient.lastName) "
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.SexLabel.text = patient.sex
        self.HeightLabel.text = String(patient.height)
        self.EthnicityLabel.text = patient.ethnicity
        self.WeightLabel.text = String(patient.weight)
        self.EmailLabel.text = patient.email
        self.CountryLabel.text = patient.country
        
        for disease in patient.diseases{
            self.userDiseases.addTag(disease)
            self.userDiseases.textFont = UIFont.systemFont(ofSize: 18)
        }
        getReportsData()
    }
    
    //Firebase DB subscription methods
    func getReportsData(){
        ReportDataHandler.subscribeToUserReports(userId: patient.id, updateData: setReportsData)
    }
    func setReportsData(reports: Array<Report>){
        self.data = []
        self.data.append(contentsOf: reports)
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SingleReportViewController {
            if let report = sender as? Report {
                destination.report = report
                destination.isDoctorViewing = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as? CardTableViewCell else {
            fatalError("No CardTableViewCell for cardCell id")
        }
        cell.backgroundColor = UIColor.init(hexFromString: "#61646c")
        // Put data into the cell
        cell.report = data[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
}
