//
//  PacientListTableViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/21/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import EmptyDataSet_Swift

class PacientListTableViewController: UITableViewController, UISearchResultsUpdating, EmptyDataSetSource, EmptyDataSetDelegate {
    
    @IBOutlet weak var PatientNameTitleLabel: UINavigationItem!
    
    var data:Array<UserBare> = []
    var filteredTableData = [UserBare]()
    var resultSearchController = UISearchController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPatientData()
        self.tableView.delegate = self
        self.tableView.allowsSelection = true
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BetterTableViewCell")
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = data.filter{ (user) -> Bool in
            return searchPredicate.evaluate(with: "\(user.firstName) \(user.lastName)") ||  searchPredicate.evaluate(with: user.email)
        }
        filteredTableData = array
        
        self.tableView.reloadData()
    }
    
    func getPatientData(){
        UserHandler.subscribeToUserPatients(accepted: true, updateData: setPacientsData)
    }
    
    func setPacientsData(users: Array<UserBare>){
        self.data = []
        self.data.append(contentsOf: users)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            ErrorHandler.confirmDelete(view: self, messageStr: "Are you sure you want to permanently delete this patient?", onDeleteFunc: {(alert: UIAlertAction) -> Void in
//                ReportDataHandler.removeReport(userId: (Auth.auth().currentUser?.uid)!, reportId: self.data[indexPath.row].reportId)
                self.data.remove(at: indexPath.row)
                tableView.reloadData()
            })
        }
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  (resultSearchController.isActive) {
            return filteredTableData.count
        } else {
            return data.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userInfoCell", for: indexPath) as? UserRelationTableViewCell else {
            fatalError("No CardTableViewCell for cardCell id")
        }
        let user = resultSearchController.isActive ? filteredTableData[indexPath.row] : data[indexPath.row]
        cell.user = user
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserHandler.getUser(userID: data[indexPath.row].id).then { (userObj) in
            self.performSegue(withIdentifier: "showPatientsReports", sender: userObj)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        resultSearchController.searchBar.endEditing(true)
        resultSearchController.isActive = false
        if let destination = segue.destination as? PatientPageDoctorViewController {
            if let patient = sender as? User {
                destination.patient = patient
            }
        }
    }
    
    //Empty data set functions
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return -100
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Welcome"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline),
                     NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Tap + on the top right corner to start taking your first exam."
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body), NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "heartUWhite")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let ac = UIAlertController(title: "Button tapped!", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Hurray", style: .default))
        present(ac, animated: true)
    }

}
