//
//  PatientRequestsTableViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 4/21/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import EmptyDataSet_Swift

class PatientRequestsTableViewController: UITableViewController, EmptyDataSetSource, EmptyDataSetDelegate {

    var data:Array<UserBare> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPatientData()
        self.tableView.delegate = self
        self.tableView.allowsSelection = true
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }
    
    func getPatientData(){
        UserHandler.subscribeToUserPatients(accepted: false, updateData: setRequestsData)
    }
    
    func setRequestsData(users: Array<UserBare>){
        self.data = []
        self.data.append(contentsOf: users)
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as? UserRelationTableViewCell else {
            fatalError("No CardTableViewCell for cardCell id")
        }
        let user = data[indexPath.row]
        cell.user = user
        return cell
    }

    //Empty data set functions
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return -100
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Welcome"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline),
                     NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Tap + on the top right corner to start taking your first exam."
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body), NSAttributedString.Key.foregroundColor: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "heartUWhite")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let ac = UIAlertController(title: "Button tapped!", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Hurray", style: .default))
        present(ac, animated: true)
    }

}
