//
//  MainScreenViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/18/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase

class AuthScreenViewController: UIViewController {
    
    
    @IBOutlet weak var EmailTB: UITextField!
    @IBOutlet weak var PasswordTB: UITextField!
    @IBOutlet weak var LoginButton: UIButton!{
        didSet {
            LoginButton.layer.cornerRadius = 10
            LoginButton.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var SignupButton: UIButton!{
        didSet {
            SignupButton.layer.cornerRadius = 10
            SignupButton.layer.masksToBounds = true
        }
    }
    
    var handle: AuthStateDidChangeListenerHandle?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserHandler.getLoggedInUser().then { (user) in
            self.performSegueForAuthenticatedUser()
        }
//        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
//            if user != nil {
//                self.performSegueForAuthenticatedUser()
//            }
//        }
        
        let toolbar = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditing))
        EmailTB.inputAccessoryView = toolbar
        PasswordTB.inputAccessoryView = toolbar
    }
    
    @IBAction func onSignUpTap(_ sender: Any) {
        performSegue(withIdentifier: "showSignUpSegue", sender: nil)
    }
    
    @IBAction func onLoginTap(_ sender: Any) {
        if !FormHandler.verifyIfFieldsNotEmpty(view: self, textFields: EmailTB, PasswordTB){
            return
        }
        LoadingSpinner.showSpinner(onView: self.view)
        Auth.auth().signIn(withEmail: EmailTB.text!, password: PasswordTB.text!) { [weak self] user, error in
            guard let strongSelf = self else { return }
            LoadingSpinner.removeSpinner()
            if user != nil {
                self!.performSegueForAuthenticatedUser()
            }
            if let error = error {
                ErrorHandler.showErrorMessage(view: self!, error: error)
            }
        }
    }
    
    func performSegueForAuthenticatedUser(){
        UserHandler.getLoggedInUser().then({ (loggedUser) in
            if loggedUser.doctor == true {
                self.performSegue(withIdentifier: "doctorMenuSegue", sender: loggedUser)
            } else {
                self.performSegue(withIdentifier: "mainMenuSegue", sender: loggedUser)
            }
        }).catch({ (err) in
            ErrorHandler.showErrorMessage(view: self, error: err)
        })
    }
    
    
    @objc func doneEditing(){
        self.view.endEditing(true)
    }
}
