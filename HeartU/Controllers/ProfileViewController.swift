//
//  ProfileViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/19/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase
import TagListView

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userBirthday: UILabel!
    @IBOutlet weak var userCountry: UILabel!
    @IBOutlet weak var userHeight: UILabel!
    @IBOutlet weak var userWeight: UILabel!
    @IBOutlet weak var userSex: UILabel!
    @IBOutlet weak var userEthnicity: UILabel!
    @IBOutlet weak var userDiseases: TagListView!
    @IBOutlet weak var DiseasesLabel: UILabel!
    
    var ref: DatabaseReference!
    var data:User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserFromFirebase()
    }
    
    func getUserFromFirebase(){
        UserHandler.getLoggedInUser().then { (loggedUser) in
            self.userName.text = "\(loggedUser.firstName) \(loggedUser.lastName)"
            self.userEmail.text = loggedUser.email
            self.userBirthday.text = DateHandler.formatDateNoHour(toFormat: loggedUser.birthday)
            self.userCountry.text = loggedUser.country
            self.userHeight.text = "\(loggedUser.height)"
            self.userWeight.text = "\(loggedUser.weight)"
            self.userSex.text = loggedUser.sex
            self.userEthnicity.text = loggedUser.ethnicity
            if (!loggedUser.doctor){
                for disease in loggedUser.diseases{
                    self.userDiseases.addTag(disease)
                    self.userDiseases.textFont = UIFont.systemFont(ofSize: 18)
                }
            } else {
                self.DiseasesLabel.isHidden = true
            }
        }.catch { (error) in
            ErrorHandler.showErrorMessage(view: self, error: error)
        }
    }
    
    @IBAction func Logout(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "authViewController")
//            self.present(controller, animated: true, completion: nil)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let _ = storyboard.instantiateViewController(withIdentifier: "authViewController")
                self.dismiss(animated: true, completion: nil)
            
        } catch let signOutError as NSError {
            ErrorHandler.showErrorMessage(view: self, error: signOutError)
        }
    }

}
