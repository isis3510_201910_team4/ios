//
//  SignUpViewController.swift
//  HeartU
//
//  Created by Felipe Plazas on 3/18/19.
//  Copyright © 2019 HeartULimited. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var FirstNameTF: UITextField!
    @IBOutlet weak var DateTF: UITextField!
    @IBOutlet weak var LastNameTF: UITextField!
    @IBOutlet weak var EmailTF: UITextField!
    @IBOutlet weak var CountryTF: UITextField!
    @IBOutlet weak var SexTF: UITextField!
    @IBOutlet weak var HeightTF: UITextField!
    @IBOutlet weak var WeightTF: UITextField!
    @IBOutlet weak var EthnicityTF: UITextField!
    @IBOutlet weak var HeartDiseaseSW: UISwitch!
    @IBOutlet weak var HypertensionSW: UISwitch!
    @IBOutlet weak var CopdSW: UISwitch!
    @IBOutlet weak var PasswordTF: UITextField!
    @IBOutlet weak var ConfirmPasswordTF: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var countryList: Array<String> = []
    let sexList = ["Male", "Female", "I'd rather not say"]
    let raceList = ["American Indian or Alaska Native", "Asian", "Black or African American", "Native Hawaiian or Other Pacific Islander", "White", "Hispanic", "Other", "I'd rather not say"]
    
    let countryPicker = UIPickerView()
    let sexPicker = UIPickerView()
    let datePicker = UIDatePicker()
    let racePicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countryList.append(name)
        }
        
        countryPicker.delegate = self
        CountryTF.inputAccessoryView = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditingCountry))
        CountryTF.inputView = countryPicker
        
        sexPicker.delegate = self
        SexTF.inputView = sexPicker
        SexTF.inputAccessoryView = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditingSex))
        
        racePicker.delegate = self
        EthnicityTF.inputAccessoryView = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditingRace))
        EthnicityTF.inputView = racePicker
            
        showDatePicker()
        
        let toolbar = UIObjectCreator.createKeyboardToolBar(view: self, selector: #selector(doneEditing))
        FirstNameTF.inputAccessoryView = toolbar
        LastNameTF.inputAccessoryView = toolbar
        EmailTF.inputAccessoryView = toolbar
        HeightTF.inputAccessoryView = toolbar
        WeightTF.inputAccessoryView = toolbar
        PasswordTF.inputAccessoryView = toolbar
    }
    
    @objc func onBackTap(_ sender: Any) {        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitTap(_ sender: Any) {
        if !FormHandler.verifyIfFieldsNotEmpty(view: self, textFields: FirstNameTF, LastNameTF, EmailTF, HeightTF, WeightTF, DateTF, SexTF, CountryTF, EthnicityTF, PasswordTF, ConfirmPasswordTF) {
            return;
        }
        if PasswordTF.text != ConfirmPasswordTF.text {
            ErrorHandler.showErrorMessage(view: self, errorMessage: "The passwords entered do not match.")
            return
        }
        LoadingSpinner.showSpinner(onView: self.view)
        Auth.auth().createUser(withEmail: EmailTF.text!, password: PasswordTF.text!) { authResult, error in
            LoadingSpinner.removeSpinner()
            if let error = error {
                ErrorHandler.showErrorMessage(view: self, error: error)
            } else {
                self.ref = Database.database().reference()
                let userID : String = (Auth.auth().currentUser?.uid)!
                self.ref.child("users").child(userID).setValue([
                    "firstName" : self.FirstNameTF.text!,
                    "lastName"  : self.LastNameTF.text!,
                    "country"   : self.CountryTF.text!,
                    "email"     : self.EmailTF.text!,
                    "sex"       : self.SexTF.text!,
                    "height"    : self.HeightTF.text!,
                    "weight"    : self.WeightTF.text!,
                    "gender"    : self.SexTF.text!,
                    "ethnicity" : self.EthnicityTF.text!,
                    "diseases"  : FormHandler.generateListOfDiseases(switches: [self.HeartDiseaseSW, self.HypertensionSW, self.CopdSW], names: ["Heart Failure", "Hypertension", "COPD"] ),
                    "birthdate" : self.DateTF.text!
                ]){ (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        ErrorHandler.showErrorMessage(view: self, error: error)
                    }
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            return countryList.count
        } else if pickerView == sexPicker {
            return sexList.count
        } else if pickerView == racePicker {
            return raceList.count
        }
        return 0;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
            return countryList[row]
        } else if pickerView == sexPicker {
            return sexList[row]
        } else if pickerView == racePicker {
            return raceList[row]
        }
        return nil;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPicker {
            CountryTF.text = countryList[row]
        } else if pickerView == sexPicker {
            SexTF.text = sexList[row]
        } else if pickerView == racePicker {
            EthnicityTF.text = raceList[row]
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        toolbar.setItems([doneButton], animated: true)
        
        DateTF.inputAccessoryView = toolbar
        DateTF.inputView = datePicker
    }
    
    @objc func doneEditing(){
        self.view.endEditing(true)
    }
    
    @objc func doneEditingCountry(){
        let row = countryPicker.selectedRow(inComponent: 0);
        CountryTF.text = countryList[row];
        self.view.endEditing(true)
    }
    
    @objc func doneEditingRace(){
        let row = racePicker.selectedRow(inComponent: 0);
        EthnicityTF.text = raceList[row];
        self.view.endEditing(true)
    }
    
    @objc func doneEditingSex(){
        let row = sexPicker.selectedRow(inComponent: 0);
        SexTF.text = sexList[row];
        self.view.endEditing(true)
    }
    
    @objc func donedatePicker(){
        DateTF.text = DateHandler.formatDate(toFormat: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}
